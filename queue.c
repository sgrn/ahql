/**
 * \file queue.c
 * \brief generic queue library allowing to manage queues generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-03-25 Wed 10:37 PM
 * \copyright
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *              http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "queue.h"

Queue * Queue_malloc(void)
{
	QueueElement * sentinelHead;
	Queue * queue;
	sentinelHead = malloc(sizeof(QueueElement));
	if (sentinelHead == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	sentinelHead->next = NULL;
	queue = malloc(sizeof(Queue));
	if (queue == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	queue->head = sentinelHead;
	queue->tail = sentinelHead;
	queue->size = 0;
	return queue;
}

bool Queue_isempty(const Queue * const queue)
{
	return queue->size == 0;
}

QueueElement * Queue_at_head(const Queue * const queue)
{
	return queue->head->next;
}

void Queue_swap(Queue * const queue0, Queue * const queue1)
{
	Queue tmp;
	tmp.head = queue0->head;
	tmp.tail = queue0->tail;
	tmp.size = queue0->size;
	queue0->head = queue1->head;
	queue0->tail = queue1->tail;
	queue0->size = queue1->size;
	queue1->head = tmp.head;
	queue1->tail = tmp.tail;
	queue1->size = tmp.size;
}

static QueueElement * QueueElement_mallocinit(const void * const value, const size_t offset)
{
	QueueElement * element;
	element = malloc(sizeof(QueueElement));
	if (element == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (offset == 0) {
		fprintf(stderr, "error: cannot allocate cause offset equals 0.\n");
		exit(EXIT_FAILURE);
	}
	element->value = malloc(offset);
	if (element->value == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	memcpy(element->value, value, offset);
	return element;
}

void Queue_enqueue(Queue * const queue, const void * const value, const size_t offset)
{
	QueueElement * element;
	element = QueueElement_mallocinit(value, offset);
	element->next = NULL;
	queue->tail->next = element;
	queue->tail = element;
	queue->size++;
}

void Queue_dequeue(Queue * const queue)
{
	QueueElement * tmp;
	if (Queue_isempty(queue)) {
		fprintf(stderr, "error: cannot pop dequeue an empty queue.\n");
		exit(EXIT_FAILURE);
	}
	tmp = queue->head->next;
	free(tmp->value);
	queue->head->next = tmp->next;
	free(tmp);
	queue->size--;
}

void Queue_free(Queue * * const queue)
{
	while (! Queue_isempty(*queue)) {
		Queue_dequeue(*queue);
	}
	free((*queue)->head);
	free(*queue);
	*queue = NULL;
}
