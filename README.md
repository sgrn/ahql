# A handful generic queue C library

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*
## About

* This C queue library is [generic](https://en.wikipedia.org/wiki/Generic_programming).
* You can access the project documentation in the Doc/ directory.
* You can also read the next section of this page in order to understand basic usage.
* If you would like to know more about the general concept of queue [read this document](https://en.wikipedia.org/wiki/FIFO_(computing_and_electronics)).
* Feel free to submit changes.

## Usage

There is two way to access routines. The easiest is using wrapper routine with macros and the more advanced way is to call the original generic version of the routine. The following sections cover each techniques in depth.

Queue typed objects contains 2 fields:

* head which corresponds to the first element added to the queue
* tail which corresponds to the last element added to the queue
* size which is the current number of value contained inside the queue.

### Wrapper routine

This method allows you to call macros to define specially typed structures and routines.

#### Macro syntax

```
QUEUE_[ROUTINE IDENTIFIER](<TYPE>, <NAME>)
```

Where *[ROUTINE IDENTIFIER]* is the upper case routine name, if no routine name specified then it declares the specially typed queue structure, *\<TYPE\>* the queue type, *\<NAME\>* an identifier.  
To learn more about each macro read the documentation.

#### Example

```C
#include <stdio.h>
#include "queue.h"

QUEUE(int, Int)
QUEUE_MALLOC(int, Int)
QUEUE_ISEMPTY(int, Int)
QUEUE_SET_FRONT(int, Int)
QUEUE_GET_FRONT(int, Int)
QUEUE_ENQUEUE(int, Int)
QUEUE_DEQUEUE(int, Int)
QUEUE_FREE(int, Int)

int main (void)
{
	QueueInt * queue;
	queue = QueueInt_malloc();
	QueueInt_enqueue(queue, 5);
	QueueInt_enqueue(queue, 12);
	QueueInt_enqueue(queue, 90);
	printf("%d\n", QueueInt_get_front(queue));
	QueueInt_set_front(queue, 100);
	printf("%d\n", QueueInt_get_front(queue));
	QueueInt_dequeue(queue);
	printf("%d\n", QueueInt_get_front(queue));
	QueueInt_dequeue(queue);
	printf("%d\n", QueueInt_get_front(queue));
	QueueInt_dequeue(queue);
	if (QueueInt_isempty(queue)) {
		printf("Queue is empty.\n");
	}
	QueueInt_free(&queue);
	return 0;
}
```

### Generic routine

This method allows you to call generic routine directly, however you will need to handle casting manually.  
To learn more about each generic routine you can read the documentation.

#### Exemple

```C
#include <stdio.h>
#include "queue.h"

int main (void)
{
	Queue * queue;
	int tab[3] = {5, 12, 90};
	queue = Queue_malloc();
	Queue_enqueue(queue, tab, sizeof(int));
	Queue_enqueue(queue, tab + 1, sizeof(int));
	Queue_enqueue(queue, tab + 2, sizeof(int ));
	printf("%d\n", *((int *)(Queue_at_front(queue)->value)));
	*((int *)queue->last->value) = 100;
	printf("%d\n", *((int *)(Queue_at_front(queue)->value)));
	Queue_dequeue(queue);
	printf("%d\n", *((int *)(Queue_at_front(queue)->value)));
	Queue_dequeue(queue);
	printf("%d\n", *((int *)(Queue_at_front(queue)->value)));
	Queue_dequeue(queue);
	if (Queue_isempty(queue)) {
		printf("Queue is empty.\n");
	}
	Queue_free(&queue);
	return 0;
}
```
